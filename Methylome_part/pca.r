############################################
#install.packages("ggplot2")
#install.packages("ggfortify")
#install.packages("GenomicRanges")
#if (!require("BiocManager", quietly = TRUE))
#  install.packages("BiocManager")
#install.packages("mixOmics")
#BiocManager::install("GenomicRanges")
library("ggplot2")
library("ggfortify")
library("GenomicRanges")
library("mixOmics")

data = read.table("probes_mapping_ilot.txt",sep="\t",header=T)
y = data[,4:19]
ompleterecords <- na.omit(y) 
myacp2 <- prcomp(t(ompleterecords), scale = FALSE) 
myacp2

summary(myacp2)
autoplot(myacp2, label = TRUE, label.size = 1)
