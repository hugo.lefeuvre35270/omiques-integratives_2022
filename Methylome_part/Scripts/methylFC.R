BiocManager::install("limma")
BiocManager::install("Biobase")
BiocManager::install("ggplot2")

library("ggplot2")
library("limma")


matrixMet= as.matrix(read.table("data\\probes_mapping_ilot.txt", row.names=2, header=T, sep="\t"))
matrixMet = na.omit(matrixMet)
summary(matrixMet)
head(matrixMet)
submatrixMet=matrixMet[,-c(1,2,3,20,21)]
head(submatrixMet)
rn=rownames(submatrixMet)
submatrixMet=apply(submatrixMet,2, as.numeric)
rownames(submatrixMet)=rn
head(rn)
summary(submatrixMet)
head(submatrixMet)


design= model.matrix(~ 0+factor(c(1,1,1,2,2,2,2,3,3,3,2,2,2,2,3,3)))
colnames(design)=c("Control","IDH_Wild_Type", "IDH_Wild_Mutant")
design
cont_matrix = makeContrasts(ContVsIDHwt= Control-IDH_Wild_Type,
                            ContVsIDHmut= Control-IDH_Wild_Mutant, levels=design)
cont_matrix

head(submatrixMet)
fit= lmFit(submatrixMet, design)
head(coef(fit))
fit_contrast=contrasts.fit(fit,cont_matrix)
fit_contrast
fit_contrast <- eBayes(fit_contrast)
volcanoplot(fit_contrast)


CvsWT=topTable(fit_contrast, coef="ContVsIDHwt", number=Inf)
head(CvsWT[order(CvsWT$logFC,decreasing=FALSE),],50)
head(CvsWT[order(CvsWT[,1],decreasing=TRUE),],50)
filtCvsWT=CvsWT[CvsWT$adj.P.Val < 0.05,]
#Up and Down
filtCvsWT=filtCvsWT[filtCvsWT$logFC > 0.4 | filtCvsWT$logFC < -0.4,]
head(filtCvsWT)
matrixCvsWT=matrixMet[rownames(filtCvsWT),]
matrixCvsWT <- cbind(rownames(matrixCvsWT), data.frame(matrixCvsWT, row.names=NULL))
rownames(matrixCvsWT)=matrixCvsWT$row.names
matrixCvsWT=matrixCvsWT[,-2]
colnames(matrixCvsWT)[1]="TargetID"
head(matrixCvsWT)
write.table(matrixCvsWT, file="probes_mapping_CvsWT.txt", quote=FALSE, sep="\t")
head(filtCvsWT)
#Up
filtCvsWTup=filtCvsWT[filtCvsWT$logFC > 0.4,]
head(filtCvsWTup)
matrixCvsWTup=matrixMet[rownames(filtCvsWTup),]
matrixCvsWTup <- cbind(rownames(matrixCvsWTup), data.frame(matrixCvsWTup, row.names=NULL))
rownames(matrixCvsWTup)=matrixCvsWTup$row.names
matrixCvsWTup=matrixCvsWTup[,-2]
colnames(matrixCvsWTup)[1]="TargetID"
head(matrixCvsWTup)
write.table(matrixCvsWTup, file="probes_mapping_CvsWT_up.txt", quote=FALSE, sep="\t")
#Down
filtCvsWTdown=filtCvsWT[filtCvsWT$logFC < -0.4,]
head(filtCvsWTdown)
matrixCvsWTdown=matrixMet[rownames(filtCvsWTdown),]
matrixCvsWTdown <- cbind(rownames(matrixCvsWTdown), data.frame(matrixCvsWTdown, row.names=NULL))
rownames(matrixCvsWTdown)=matrixCvsWTdown$row.names
matrixCvsWTdown=matrixCvsWTdown[,-2]
colnames(matrixCvsWTdown)[1]="TargetID"
head(matrixCvsWTdown)
write.table(matrixCvsWTdown, file="probes_mapping_CvsWT_down.txt", quote=FALSE, sep="\t")

#x11()
CvsWT$diffExpr= "NO"
CvsWT$diffExpr[CvsWT$logFC > 0.4 & CvsWT$adj.P.Val < 0.05] = "UP"
CvsWT$diffExpr[CvsWT$logFC < -0.4 & CvsWT$adj.P.Val < 0.05] = "DOWN"
p1 = ggplot(data=CvsWT, aes(x=logFC, y=-log10(adj.P.Val), col=diffExpr)) + geom_point() + theme_minimal()
p1 = p1 + geom_vline(xintercept=c(-0.4, 0.4), col="red") + geom_hline(yintercept=-log10(0.05), col="red")
mycolors = c("blue", "red", "#009E73")
names(mycolors) = c("DOWN", "UP", "NO")
p1 = p1+ scale_colour_manual(values = mycolors)
p1 = p1+ labs(title="Volcano Plot of Differential methylation of IDHwt against Control",
              x= "Log of Fold Change", y="Log of adjusted p-value", colour="Differential \n  expression")
p1
ggsave("VolcanoPlot_CvsIDHwt.jpeg",p1, device="jpeg")




CvsMUT=topTable(fit_contrast, coef="ContVsIDHmut", number=Inf)
head(CvsMUT[order(CvsMUT[,1],decreasing=FALSE),],50)
head(CvsMUT[order(CvsMUT[,1],decreasing=TRUE),],50)
filtCvsMUT=CvsMUT[CvsMUT$adj.P.Val < 0.05,]
#Up and Down
filtCvsMUT=filtCvsMUT[filtCvsMUT$logFC > 0.4 | filtCvsMUT$logFC < -0.4,]
filtCvsMUT
matrixCvsMUT=matrixMet[rownames(filtCvsMUT),]
matrixCvsMUT <- cbind(rownames(matrixCvsMUT), data.frame(matrixCvsMUT, row.names=NULL))
rownames(matrixCvsMUT)=matrixCvsMUT$row.names
matrixCvsMUT=matrixCvsMUT[,-2]
colnames(matrixCvsMUT)[1]="TargetID"
head(matrixCvsMUT)
write.table(matrixCvsMUT, file="probes_mapping_CvsMUT.txt", quote=FALSE, sep="\t")
#Up
filtCvsMUTup=filtCvsMUT[filtCvsMUT$logFC > 0.4,]
filtCvsMUTup
matrixCvsMUTup=matrixMet[rownames(filtCvsMUTup),]
matrixCvsMUTup <- cbind(rownames(matrixCvsMUTup), data.frame(matrixCvsMUTup, row.names=NULL))
rownames(matrixCvsMUTup)=matrixCvsMUTup$row.names
matrixCvsMUTup=matrixCvsMUTup[,-2]
colnames(matrixCvsMUTup)[1]="TargetID"
head(matrixCvsMUTup)
write.table(matrixCvsMUTup, file="probes_mapping_CvsMUT_up.txt", quote=FALSE, sep="\t")
#Down
filtCvsMUTdown=filtCvsMUT[filtCvsMUT$logFC < -0.4,]
filtCvsMUTdown
matrixCvsMUTdown=matrixMet[rownames(filtCvsMUTdown),]
matrixCvsMUTdown <- cbind(rownames(matrixCvsMUTdown), data.frame(matrixCvsMUTdown, row.names=NULL))
rownames(matrixCvsMUTdown)=matrixCvsMUTdown$row.names
matrixCvsMUTdown=matrixCvsMUTdown[,-2]
colnames(matrixCvsMUTdown)[1]="TargetID"
head(matrixCvsMUTdown)
write.table(matrixCvsMUTdown, file="probes_mapping_CvsMUT_down.txt", quote=FALSE, sep="\t")

CvsMUT$diffExpr= "NO"
CvsMUT$diffExpr[CvsMUT$logFC > 0.4 & CvsMUT$adj.P.Val < 0.05] = "UP"
CvsMUT$diffExpr[CvsMUT$logFC < -0.4 & CvsMUT$adj.P.Val < 0.05] = "DOWN"
p2 = ggplot(data=CvsMUT, aes(x=logFC, y=-log10(adj.P.Val), col=diffExpr)) + geom_point() + theme_minimal()
p2 = p2 + geom_vline(xintercept=c(-0.4, 0.4), col="red") + geom_hline(yintercept=-log10(0.05), col="red")
mycolors = c("blue", "red", "#009E73")
names(mycolors) = c("DOWN", "UP", "NO")
p2 = p2+ scale_colour_manual(values = mycolors)
p2 = p2+ labs(title="Volcano Plot of Differential methylation of IDHmut against Control",
              x= "Log of Fold Change", y="Log of adjusted p-value", colour="Differential \n  expression")
p2
ggsave("VolcanoPlot_CvsIDHmut.jpeg",p2, device="jpeg")



# CvsMUT
# rownames(CvsMUT)=CvsMUT[,"ID"]
# CvsMUT[CvsMUT$ID %in% rownames(fit_contrast)]
# CvsMUT["1.310414e-02",]
# 
# rownames(CvsMUT)
# a=rownames(fit_contrast)
# head(a)
# typeof(a)
# attributes(a)
# yp=as.data.frame(a)
# yo=as.data.frame(a)
# head(typeof(yo))
# 
# yp=cbind(yp,rownames(yp))
# head(yp)
# yp$ID=as.character(yp$ID)
# colnames(yp)=c("ID","targetID")
# rownames(yp)=yp$ID
# head(yp$ID)
# yp[yp$ID == "0.000000e+00",]
# 
# unique(yp$ID[duplicated(yp$ID)])
# 
# CvsMUT[CvsMUT$ID %in% rownames(yp)]$ID
# falseID=CvsMUT[CvsMUT$ID %in% rownames(yp)]$ID
# falseID
# rownames(CvsMUT[falseID,])=yp[falseID,2]
# ahah=yp[falseID,2]
# print(ahah)
# as.list(head(yp))
# head(CvsMUT)
# 
# 
# 
# head(rownames(fit_contrast))
# head(fit_contrast)
# 
# summary(fit_contrast)
# head(fit_contrast$coefficients,50)
# 
# yp$names= rownames(yp)
# head(rownames(yp))
# yp$names= rownames(yp)
# head(yp)
