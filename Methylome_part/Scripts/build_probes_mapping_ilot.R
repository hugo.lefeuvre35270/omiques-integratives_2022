if (!require("BiocManager", quietly = TRUE))
  install.packages("BiocManager")

BiocManager::install("GenomicRanges")
install.packages("dplyr")
library(GenomicRanges)
library(dplyr)


#on charge fichier avec position des îlots CpG
pos_ilot=read.delim2("cpgIslandExt.txt",header=F)
head(pos_ilot)
unique(pos_ilot$V2)

#on charge fichier avec position des sondes et taux de méthylation
methyl_sonde=read.table("HM450k_filtered_norm_443691CpG.txt", header = TRUE)
#change nom des colonnes
colnames(methyl_sonde)<-c("TargetID", "CHR", "start","C1","C2","C3","IDHwt1","IDHwt2","IDHwt3","IDHwt4","IDHmut1","IDHmut2","IDHmut3","IDHwt5","IDHwt6","IDHwt7","IDHwt8","IDHmut4","IDHmut5")
colnames(methyl_sonde)
head(methyl_sonde)
unique(methyl_sonde$CHR)
#on ajoute position de fin de la sonde pour pouvoir utiliser grange
methyl_sonde$CHR=paste("chr",methyl_sonde[,2],sep="")
methyl_sonde$end=methyl_sonde$start+1
head(methyl_sonde)

#on crée les objets range associés
#position des sondes
grsonde=GRanges(seqnames = methyl_sonde$CHR, IRanges( start= methyl_sonde$start, width = 1),TargetID=methyl_sonde$TargetID)
grsonde
#position des ilots CpG
grilotcpg=GRanges(seqnames = pos_ilot$V2, IRanges(start = pos_ilot$V3, end = pos_ilot$V4))
grilotcpg


#on fait dans l'autre sens pour avoir liste des sondes qui ne mappent que dessus
mcols(grsonde)$countilot<-countOverlaps (grsonde,grilotcpg)
grsonde$countilot
#on ajoute comptage des ilots au tableau des sondes
methyl_sonde$countilot = grsonde$countilot
methyl_sonde
methyl_sonde_ilot=methyl_sonde%>%filter(countilot>=1)
#Utiliser ce ficheir pour faire la différenciation et l'ACP
methyl_sonde_ilot
write.table(methyl_sonde_ilot, file = "probes_mapping_ilot.txt", sep = "\t", quote = FALSE, row.names = TRUE, col.names = TRUE)
