# Master 2 Bio-Informatics  
## Data analysis and modeling path  - UE - Omiques intégratives

## intégration des données issues de la transcriptomique et le méthylome

Presented by :
Hugo LEFEUVRE,
Nour LARIFI,
Yareni DURANTHON (née PEREZ),
François HIRIART.



## Plan de travaille 
1. introduction
2. Objectif
3. Question Biologique 
4. Matériel Biologique 
5. WorkFlow
6. Méthylome: Taux de méthylation CpG sur l' ADN 
7. Transcriptomique : Expression des gènes 

## Introduction 
Le cancer est une maladie complexe qui aboutit à la perturbation de certaines voies clés de l'organisme. 
La méthylation de l'ADN est couramment associée à des altérations transcriptomiques, notamment dans les cellules cancéreuses ; ces aberrations de la méthylation de l'ADN peuvent avoir des conséquences majeures pour l'homme. 
L'hyperméthylation et l'hypométhylation de l'ADN des îlots CpG sont considérées comme l'une des principales causes de l'expression anormale des gènes. L'une des principales conséquences de cet état serait l'extinction de certains gènes suppresseurs de tumeurs tels que le BRCA1.
Il existe plusieurs types d'altérations épigénétiques, telles que des modifications de la méthylation/enhancer au niveau des îlots CpG, qui peuvent entraîner des modifications de l'expression génétique. (Court et. al 2021)

## Objectif 
Construction d'un workflow pour l'analyse du transcriptomique vs méthylome pour déterminer si le différentiel de méthylation au niveau des îlots CpG associés à des enhancers explique le différentiel d’expression des gènes associés aux enhancers

## Question Biologique 
Est-ce que le différentiel de méthylation au niveau des îlots CpG associés à des enhancers explique le différentiel d’expression des gènes associés aux enhancers ?
Traduction : Impact du différentiel de méthylation des enhancers associés aux ilôts CpG sur le différentiel d’expression des gènes associés aux enhancers


## Matériel Biologique 
échantillons de cerveaux avec de gliome et cerveaux sains 
8 échantillons wt, décrit comme le gliome le plus agressif → pour les identifier : GBM_4, GBM_7, GBM_27, GBM_34, GIII_4, GIII_11, GIII_12 et GIII_17.
5 échantillons mut, décrit comme le gliome le plus agressif → pour les identifier : GII_2, GII_4, GII_5, GIII_22 et GIII_24.
3 échantillons ctrl, prélevés sur des cerveaux sans gliome → C_1, C_4, et CF_4SB.

# WorkFlow:
## Méthylome: Taux de méthylation CpG sur l' ADN 

Les échantillons ont été soumis à une ADN pouce HM450 qui interroge le statut de méthylation de plus de 450 000 cytocytes sur l'ensemble du génome. Par la suite, à ete realise une Conversion bisulfate et séquençage avec le protocole de méthylation illumina infinimum HD, pour appliquer une Normalisation Qualité et Nettoyage avec l’outil GenomeStudio version 2011.1, ensuit, un Mapping sur le génome de référence : génome hg19 annotations sur GENCODE V19 a été effectue avec le fin de creer un fichier:  HM450k_filtered_norm_443691CpG.txt: données filtrées.



## Transcriptomique : Expression des gènes 

A partir de les 16 échantillons  ils sont obtenu l’ARN-total après déplétion de l’ARN ribosomal qui a été soumis à un Séquençage par Illumina (Ribo-Zéro rRNA Removal Kit, Illumina), afin de procéder à un  Mapping : ARN-Seq mappe sur le génome humain (hg19) avec l’outil TopHat2 et un fichier d’annotation GENCODE, et continuer avec un Comptage de lectures appariées : script Htseq-count + niveau d’expression génique FPKM détermine avec les outils : Cuffquant, Cuffnorm et Cufflink, cecu avec le but d'obtenir un fichier: RNAseq_GSE123892_counts_gencode_V19_2_court :  “counts” (contenant le nombre de reads) d’expériences d'expression génique avec annotation complète. Ce fichier sera soumis à une analyse d'expression différentielle DEGs : avec l'outil DESeq2 qui nous donnera une liste de gènes différentiellement exprimés up et down. 


